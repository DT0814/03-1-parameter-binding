package com.twuc.webApp.web.controller;

import org.springframework.web.bind.annotation.*;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api/request/test")
public class RequestParamController {
    @PostMapping
    public String test(@RequestParam("name") String name,@RequestParam("age")Integer age){
        return name+age;
    }
    @PostMapping("/default")
    public String testDefault(@RequestParam(value = "name",defaultValue = "zhangSan") String name,@RequestParam(value = "age",defaultValue = "12")Integer age){
        return name+age;
    }
}
