package com.twuc.webApp.web.controller;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api/student")
public class StudentController {
    @GetMapping
    public String getTest(@RequestParam(name = "gender") String gender
            , @RequestParam(name = "class") String className) {
        return "I am a " + gender + " ,my class name " + className;
    }

    @PostMapping
    public String post(@RequestBody @Valid Student student) {
        return student.getName();
    }

    static class Student {
        @NotNull
        @Size(max = 6, min = 2)
        private String name;

        @Max(200)
        @Min(1)
        private Integer age;
        @Email()
        private String email;

        public Student() {
        }

        public Student(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }
}
