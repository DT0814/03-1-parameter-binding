package com.twuc.webApp.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @GetMapping("/{userId}")
    public void testType(@PathVariable(name = "userId") Integer userid) {

    }


    @GetMapping("/int/test/{userId}")
    public int testType(@PathVariable(name = "userId") int userid) {
        return userid;
    }
    @GetMapping("/{userId}/books/{bookId}")
    public String testMany(@PathVariable(name = "userId") Integer userid, @PathVariable(name = "bookId") Integer bookid) {
        return userid + "" + bookid;
    }
}
