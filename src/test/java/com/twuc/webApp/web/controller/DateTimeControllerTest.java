package com.twuc.webApp.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class DateTimeControllerTest {
    @Autowired
    MockMvc mockMvc;


    @Test
    void test_date_request() throws Exception {

        mockMvc.perform(post("/api/datetimes")
                .content("{ \"dateTime\": \"2019-10-01T10:00:00Z\" }")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().string("2019"));
        mockMvc.perform(post("/api/datetimes")
                .content("{ \"dateTime\": \"2019-10-01\" }")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is4xxClientError());

    }
}
