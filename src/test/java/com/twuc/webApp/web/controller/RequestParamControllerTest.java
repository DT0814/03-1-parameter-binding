package com.twuc.webApp.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@SpringBootTest
@AutoConfigureMockMvc
public class RequestParamControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void test_request_param() throws Exception {
        mockMvc.perform(post("/api/request/test?name=zhangSan&age=12"))
                .andExpect(content().string("zhangSan12"));
    }
    @Test
    void test_request_param_default() throws Exception {
        mockMvc.perform(post("/api/request/test/default"))
                .andExpect(content().string("zhangSan12"));
    }
}
