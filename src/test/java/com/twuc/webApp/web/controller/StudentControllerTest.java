package com.twuc.webApp.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.MimeTypeUtils;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void test_get_info() throws Exception {
        mockMvc.perform(
                get("/api/student")
                        .param("gender", "female")
                        .param("class", "A"))
                .andExpect(content().string("I am a female ,my class name A"));
    }

    @Test
    void test_request_body() throws Exception {
        mockMvc.perform(
                post("/api/student")
                        .content("{\"name\":\"zhangSan\",\"age\":18}")
                        .contentType(MimeTypeUtils.APPLICATION_JSON_VALUE))
                .andExpect(content().string("zhangSan"));
    }

    @Test
    void test_valid() throws Exception {
        mockMvc.perform(
                post("/api/student")
                        .content("{\"name\":\"zhangSan\",\"age\":18}"))
                .andExpect(status().is4xxClientError());
        mockMvc.perform(
                post("/api/student")
                        .content("{\"name\":\"a\",\"age\":18}"))
                .andExpect(status().is4xxClientError());
        mockMvc.perform(
                post("/api/student")
                        .content("\"age\":18}"))
                .andExpect(status().is4xxClientError());
        mockMvc.perform(
                post("/api/student")
                        .content("{\"name\":\"zhan\",\"age\":0}"))
                .andExpect(status().is4xxClientError());
        mockMvc.perform(
                post("/api/student")
                        .content("{\"name\":\"zhan\",\"age\":201}"))
                .andExpect(status().is4xxClientError());
        mockMvc.perform(
                post("/api/student")
                        .content("{\"name\":\"zhan\",\"age\":200}"))
                .andExpect(status().is4xxClientError());
        mockMvc.perform(
                post("/api/student")
                        .content("{\"name\":\"zhan\",\"age\":1}"))
                .andExpect(status().is4xxClientError());

        mockMvc.perform(
                post("/api/student")
                        .content("{\"name\":\"zhan\",\"emile\":0}"))
                .andExpect(status().is4xxClientError());
    }



}
