package com.twuc.webApp.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void test_path_variable_type() throws Exception {
        mockMvc.perform(get("/api/users/acd"))
                .andExpect(status().is4xxClientError());
        mockMvc.perform(get("/api/users/123"))
                .andExpect(status().isOk());
    }
    @Test
    void test_path_variable_type_primitive() throws Exception {
        mockMvc.perform(get("/api/users/int/test/123"))
                .andExpect(status().isOk())
                .andExpect(content().string("123"));
    }
    @Test
    void test_path_many() throws Exception {
        mockMvc.perform(get("/api/users/123/books/456"))
                .andExpect(status().isOk())
                .andExpect(content().string("123456"));

    }
}
